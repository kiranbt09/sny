// call the packages we need
var express = require('express');        // call express
var app = express();                 // define our app using express
var bodyParser = require('body-parser');
var morgan = require('morgan');
var config = require('./config');
var mongoose = require('mongoose');
var fileUpload = require('express-fileupload');

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(morgan('dev'));
app.set('view engine', 'ejs');
app.use(fileUpload());

var cors = {
    origin: ["http://localhost:4200","http://localhost:8100"],
    default: "http://localhost:4200"
}
app.use(function (req, res, next) {


    var origin = cors.origin.indexOf(req.header('host').toLowerCase()) > -1 ? req.headers.origin : cors.default;
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', origin);
    // res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8100');
    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);
    // Pass to next layer of middleware
    if ('OPTIONS' === req.method) {
        //respond with 200
        res.sendStatus(200);
    } else {
        //move on
        next();
    }
});



//Serves all the request which includes /images in the url from Images folder
app.use('/assets', express.static(__dirname + '/assets'));

// mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/sny',function(error,data){
if(error)
console.log("Failed to connect to DB");
else
console.log("DB Connected Succesfully");
});

//Test
//Development
//Prod


// set our port
var port = process.env.PORT || config.port;
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

// Version Controls
var user_v1 = require('./app/versions/v1/routes/user_routes');
var business_v1 = require('./app/versions/v1/routes/business_routes');
app.use('/api/v1/user', user_v1);
app.use('/api/v1/business', business_v1);

app.listen(port, function () {
    console.log("Server is Running on" + port);
    console.log(config.base_url);
});