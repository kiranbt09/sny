var User = require("../../../models/user_model");
var moment = require('moment');
var bcrypt = require('bcryptjs');
var config = require("../../../../config");
var sms = require("../../../libraries/communication/sms");
var sms_messages = require("../../../libraries/messages/sms");
var response_messages = require("../../../libraries/messages/response");
var vsprintf = require("sprintf-js").vsprintf;

exports.create_user_promise = function (data) {
    return new Promise(function (resolve, reject) {
        User.findOne({ phone: data.phone }, function (error, user) {
            if (user) {
                var error = {
                    message: "User " + response_messages.already_exist,
                    key: "phone"
                };
                reject(error);
            } else {
                var user = new User();
                var salt = bcrypt.genSaltSync(config.password.salt);
                var hashedPassword = bcrypt.hashSync(data.password, salt);
                user.name = data.name;
                user.phone = data.phone;
                user.password = hashedPassword;
                user.created = moment().format('YYYY-MM-DD HH:mm:ss');
                user.save(function (err) {
                    console.log(err);
                    if (err) {
                        var error = {
                            message: response_messages.db_error,
                            key: "DB"
                        };
                        reject(error);
                    }
                    exports.send_user_otp_promise(data);
                    resolve(user);
                })
            }
        })

    });
};
exports.login_user_callback = function (data, callback) {
    User.findOne({ phone: data.phone }, callback);
}
exports.send_user_otp_promise = function (data) {
    return new Promise(function (resolve, reject) {
        var response;
        User.findOne({ phone: data.phone }, function (error, user) {
            if (user) {
                config.console(user);
                var otp_value = Math.floor(1000 + Math.random() * 9000);
                var otp_expiry = moment().add(config.otp_expiry, 'm').format('YYYY-MM-DD HH:mm:ss');
                user.otp.phone.code = otp_value;
                user.otp.phone.expiry = otp_expiry;
                user.otp.email.code = otp_value;
                user.otp.email.expiry = otp_expiry;
                exports.update_uesr(user, user._id, function (err) {
                    config.console(err);
                    if (!err) {
                        var otp_msg = vsprintf(sms_messages.otp_verify_user, [otp_value]);
                        sms.send(user.phone, otp_msg);
                        var response = {
                            status: true,
                            message: "User " + response_messages.verfy_success
                        };
                        resolve(response);
                    } else {
                        response = {
                            status: false,
                            message: response_messages.db_error,
                            key: "DB"
                        };
                        reject(response);
                    }
                });

            } else {
                response = {
                    status: false,
                    message: "User " + response_messages.does_not_exist,
                    key: "phone"
                };
                reject(response);
            }
        });

    });
};

exports.verify_user_promise = function (data, callback) {
    config.console(data);
    return new Promise(function (resolve, reject) {
        User.findOne({ phone: data.phone, 'otp.phone.code': data.otp, 'otp.phone.expiry': { '$gte': moment().format('YYYY-MM-DD HH:mm:ss') } }, function (error, user) {
            if (user) {
                user.status.phone = true;
                user.save(function (err) {
                    if (err) {
                        var error = {
                            message: response_messages.db_error,
                            key: "DB"
                        };
                        reject(error);
                    } else {
                        var data = {
                            message: "User " + response_messages.verfy_success
                        };
                        resolve(user);
                    }
                })
            } else {
                var error = {
                    message: "OTP " + response_messages.does_not_match,
                    key: "otp"
                };
                reject(error);
            }

        });
    });
};

exports.reset_user_password = function (data, callback) {
    return new Promise(function (resolve, reject) {
        exports.verify_user_promise(data).then(function (user) {
            var salt = bcrypt.genSaltSync(config.password.salt);
            var hashedPassword = bcrypt.hashSync(data.password, salt);
            user.password = hashedPassword;            
            exports.update_uesr(user,user._id,function(err){
                if(err){
                    var error = {
                            message: response_messages.db_error,
                            key: "DB"
                        };
                        reject(error);
                }else{
                    var data = {
                        message: response_messages.password_changed
                    };
                    resolve(user);
                }
            });
        }).catch(function (failure) {
            reject(failure);
        });
    });
}

exports.compare_password = function (password, hash) {
    if (bcrypt.compareSync(password, hash)) {
        return true;
    } else {
        return false;
    }
};

exports.update_uesr = function (data, user_id, callback) {
    data.save(callback)
};
exports.get_user_by_id = function (_id, callback) {
    User.findOne({ _id: _id }, callback);
}
