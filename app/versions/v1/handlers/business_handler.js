var Business = require("../../../models/business_model");
var Product = require("../../../models/product_model");
var Category = require("../../../models/product_type_model");
var Order = require("../../../models/order_model");
var moment = require('moment');
var bcrypt = require('bcryptjs');
var config = require("../../../../config");
var sms = require("../../../libraries/communication/sms");
var sms_messages = require("../../../libraries/messages/sms");
var response_messages = require("../../../libraries/messages/response");
var vsprintf = require("sprintf-js").vsprintf;

exports.create_business_promise = function (data) {
    return new Promise(function (resolve, reject) {
        Business.findOne({ business_name: data.business_name }, function (error, business) {
            if (business) {
                var error = {
                    message: "Business " + response_messages.already_exist,
                    key: "business"
                };
                reject(error);
            } else {

                var admin_codes = config.auth_token_secret;
                if (admin_codes.indexOf(data.admin_code) == -1) {
                    var error = {
                        message: "Admin code " + response_messages.does_not_match,
                        key: "admin_code"
                    };
                    reject(error);
                }
                var business = new Business();
                var salt = bcrypt.genSaltSync(config.password.salt);
                var hashedPassword = bcrypt.hashSync(data.password, salt);
                business.business_name = data.business_name;
                business.owner_name = data.owner_name;
                business.area_name = data.area_name;
                business.phone = data.phone;
                if (business.email)
                    business.email = data.email;
                business.status.phone = true;
                business.password = hashedPassword;
                business.created = moment().format('YYYY-MM-DD HH:mm:ss');
                business.save(function (err) {
                    console.log(err);
                    if (err) {
                        var error = {
                            message: response_messages.db_error,
                            key: "DB"
                        };
                        reject(error);
                    }
                    // exports.send_user_otp_promise(data);
                    resolve(business);
                })
            }
        })

    });
};

exports.create_product_type_promise = function (data) {
    console.log(data)
    return new Promise(function (resolve, reject) {
        Category.findOne({ name: data.name }, function (error, product_type) {
            if (product_type) {
                var error = {
                    message: "name " + response_messages.already_exist,
                    key: "name"
                };
                reject(error);
            } else {
                var product_type = new Category();
                console.log("asdsd",product_type)
                product_type.name = data.name;
                product_type.status = data.status;

                if (product_type.name)
                product_type.save(function (err) {
                    console.log(err);
                    if (err) {
                        var error = {
                            message: response_messages.db_error,
                            key: "DB"
                        };
                        reject(error);
                    }
                    // exports.send_user_otp_promise(data);
                    resolve(product_type);
                })
            }
        })

    });
};

exports.login_user_callback = function (data, callback) {
    config.console(data);
    Business.findOne({ phone: data.phone }, callback);
}

exports.create_product_promise = function (data) {
    return new Promise(function (resolve, reject) {
        Product.findOne({ product_name: data.product_name }, function (error, business) {

            if (business != null) {
                var error = {
                    message: "Product " + response_messages.already_exist,
                    key: "product"
                };
                reject(error);
            } else {
                con
                var product = new Product();
                product.product_name = data.product_name;
                product.product_image_url = data.product_image_url;
                product.product_price = data.product_price;
                product.product_quantity = data.product_quantity;
                product.product_quantity_type = data.product_quantity_type;
                product.status = true;

                product.created = moment().format('YYYY-MM-DD HH:mm:ss');
                product.save(function (err) {
                    console.log(err);
                    if (err) {
                        var error = {
                            message: response_messages.db_error,
                            key: "DB"
                        };
                        reject(error);
                    }
                    // exports.send_user_otp_promise(data);
                    resolve(product);
                })
            }
        })

    });
};
exports.update_product_promise = function (data) {
    return new Promise(function (resolve, reject) {
        Product.findOne({ _id: data.product_id }, function (error, product) {

            if (product == null) {
                var error = {
                    message: "Product " + response_messages.does_not_exist,
                    key: "product"
                };
                reject(error);
            } else {

                //var product = new Product();
                product.product_name = data.product_name;
                if(data.product_image_url != "")
                product.product_image_url = data.product_image_url;
                product.product_price = data.product_price;
                product.product_quantity = data.product_quantity;
                product.product_quantity_type = data.product_quantity_type;
                product.status = true;

                //product.created = moment().format('YYYY-MM-DD HH:mm:ss');
                product.save(function (err) {
                    console.log(err);
                    if (err) {
                        var error = {
                            message: response_messages.db_error,
                            key: "DB"
                        };
                        reject(error);
                    }
                    resolve(product);
                })
            }
        })

    });
};

exports.get_products = function () {

    return new Promise(function (resolve, reject) {
        Product.find({ status: true }, function (error, products) {

            if (products == null) {
                var error = {
                    message: "Products " + response_messages.does_not_exist,
                    key: "product"
                };
                reject(error);
            } else {
                var result = {
                    count : products.length,
                    list:products

                }
                resolve(result);
            }
        });
    });
}


exports.get_products_types = function () {
    return new Promise(function (resolve, reject) {
        Category.find({ status: true }, function (error, products) {

            if (products == null) {
                var error = {
                    message: "Product types " + response_messages.does_not_exist,
                    key: "product"
                };
                reject(error);
            } else {
                var result = {
                    count : products.length,
                    list:products

                }
                resolve(result);
            }
        });
    });
}


exports.send_user_otp_promise = function (data) {
    return new Promise(function (resolve, reject) {
        var response;
        User.findOne({ phone: data.phone }, function (error, user) {
            if (user) {
                config.console(user);
                var otp_value = Math.floor(1000 + Math.random() * 9000);
                var otp_expiry = moment().add(config.otp_expiry, 'm').format('YYYY-MM-DD HH:mm:ss');
                user.otp.phone.code = otp_value;
                user.otp.phone.expiry = otp_expiry;
                user.otp.email.code = otp_value;
                user.otp.email.expiry = otp_expiry;
                exports.update_uesr(user, user._id, function (err) {
                    config.console(err);
                    if (!err) {
                        var otp_msg = vsprintf(sms_messages.otp_verify_user, [otp_value]);
                        sms.send(user.phone, otp_msg);
                        var response = {
                            status: true,
                            message: "User " + response_messages.verfy_success
                        };
                        resolve(response);
                    } else {
                        response = {
                            status: false,
                            message: response_messages.db_error,
                            key: "DB"
                        };
                        reject(response);
                    }
                });

            } else {
                response = {
                    status: false,
                    message: "User " + response_messages.does_not_exist,
                    key: "phone"
                };
                reject(response);
            }
        });

    });
};
exports.verify_user_promise = function (data, callback) {
    config.console(data);
    return new Promise(function (resolve, reject) {
        User.findOne({ phone: data.phone, 'otp.phone.code': data.otp, 'otp.phone.expiry': { '$gte': moment().format('YYYY-MM-DD HH:mm:ss') } }, function (error, user) {
            if (user) {
                user.status.phone = true;
                user.save(function (err) {
                    if (err) {
                        var error = {
                            message: response_messages.db_error,
                            key: "DB"
                        };
                        reject(error);
                    } else {
                        var data = {
                            message: "User " + response_messages.verfy_success
                        };
                        resolve(user);
                    }
                })
            } else {
                var error = {
                    message: "OTP " + response_messages.does_not_match,
                    key: "otp"
                };
                reject(error);
            }

        });
    });
};

exports.reset_user_password = function (data, callback) {
    return new Promise(function (resolve, reject) {
        exports.verify_user_promise(data).then(function (user) {
            var salt = bcrypt.genSaltSync(config.password.salt);
            var hashedPassword = bcrypt.hashSync(data.password, salt);
            user.password = hashedPassword;
            exports.update_uesr(user, user._id, function (err) {
                if (err) {
                    var error = {
                        message: response_messages.db_error,
                        key: "DB"
                    };
                    reject(error);
                } else {
                    var data = {
                        message: response_messages.password_changed
                    };
                    resolve(user);
                }
            });
        }).catch(function (failure) {
            reject(failure);
        });
    });
}

exports.compare_password = function (password, hash) {
    if (bcrypt.compareSync(password, hash)) {
        return true;
    } else {
        return false;
    }
};

exports.update_uesr = function (data, user_id, callback) {
    data.save(callback)
};
exports.get_user_by_id = function (_id, callback) {
    Business.findOne({ _id: _id }, callback);
}
