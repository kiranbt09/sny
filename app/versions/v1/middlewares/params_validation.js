module.exports = {
    checkBodyParameter: function (parameters) {
        return function (request, response, next) {
            for (var i = 0; i < parameters.length; i++) {
                // console.log(request.body[parameters[i].parameter]);
                if (parameters[i].key && request.body[parameters[i].parameter] == undefined) {
                    return response.status(400).json({
                        status: false,
                        message: "Input parameter is missing!",
                        key: parameters[i].parameter
                    });
                }
                if (parameters[i].key && parameters[i].value && request.body[parameters[i].parameter].trim() == '') {
                    return response.status(400).json({
                        status: false,
                        message: "Input parameter can not be empty!",
                        key: parameters[i].parameter
                    });
                }
                if (parameters[i].key && parameters[i].value && parameters[i].validator != undefined && parameters[i].validator != '') {
                    if (!parameters[i].validator.test(request.body[parameters[i].parameter])) {
                        var message = (parameters[i].message == undefined || parameters[i].message.trim() == "") ? "Please provide valid input!" : parameters[i].message;
                        return response.status(400).json({
                            status: false,
                            message: message,
                            key: parameters[i].parameter
                        });
                    }
                }
            }
            if (i == parameters.length) {
                next();
            }
        }
    }
}