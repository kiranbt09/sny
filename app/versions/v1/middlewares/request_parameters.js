

module.exports = {
    login: [
        {
            parameter: 'phone',
            key: true,
            value: true,
            type:String,
            regular_expression:"/^\d{10}$/"
        },
        {
            parameter: 'password',
            key: true,
            value: true,
            type:String,
            regular_expression:""
        }],
    registration: [
        {
            parameter: 'name',
            key: true,
            value: true,
            type:String,
            regular_expression:""
        },
        {
            parameter: 'phone',
            key: true,
            value: true,
            type:String,
            regular_expression:""
        },
        {
            parameter: 'password',
            key: true,
            value: true,
            type:String,
            regular_expression:""
        }],
        request_otp: [
            {
                parameter: 'phone',
                key: true,
                value: true,
                type:String,
                regular_expression:""
            },
            {
                parameter: 'source',
                key: true,
                value: true,
                type:String,
                regular_expression:""
            }
        ],
    verify_otp: [
        {
            parameter: 'phone',
            key: true,
            value: true,
            type:Number,
            regular_expression:""
        },
        {
            parameter: 'otp',
            key: true,
            value: true,
            type:String,
            regular_expression:""
        }
    ],
    reset_user_password: [
        {
            parameter: 'phone',
            key: true,
            value: true,
            type:String,
            regular_expression:""
        },
        {
            parameter: 'otp',
            key: true,
            value: true,
            type:String,
            regular_expression:""
        },
        {
            parameter: 'password',
            key: true,
            value: true,
            type:String,
            regular_expression:""
        },
        {
            parameter: 'confirm_password',
            key: true,
            value: true,
            type:String,
            regular_expression:""
        }
    ],
    get_user: [
        {
            parameter: 'token',
            key: true,
            value: true,
            type:String,
            regular_expression:""
        }
    ],
    update_user: [
        {
            parameter: 'token',
            key: true,
            value: true,
            type:String,
            regular_expression:""
        },
        {
            parameter: 'email',
            key: true,
            value: false,
            type:String,
            regular_expression:""
        },
        {
            parameter: 'gender',
            key: true,
            value: false,
            type:String,
            regular_expression:""
        },
        {
            parameter: 'name',
            key: true,
            value: false,
            type:String,
            regular_expression:""
        }
    ],
    upload_user_profile_pic : [
        {
            parameter: 'token',
            key: true,
            value: true,
            type:String,
            regular_expression:""
        }
    ],
    add_business: [
        {
            parameter: 'business_name',
            key: true,
            value: true,
            type:String,
            regular_expression:""
        },
        {
            parameter: 'owner_name',
            key: true,
            value: true,
            type:String,
            regular_expression:""
        },
        {
            parameter: 'area_name',
            key: true,
            value: true,
            type:String,
            regular_expression:""
        },
        {
            parameter: 'phone',
            key: true,
            value: true,
            type:String,
            regular_expression:""
        },
        {
            parameter: 'password',
            key: true,
            value: true,
            type:String,
            regular_expression:""
        },
        {
            parameter: 'admin_code',
            key: true,
            value: true,
            type:String,
            regular_expression:""
        }
    ],
    add_product:[
        {
            parameter: 'product_name',
            key: true,
            value: true,
            type:String,
            regular_expression:""
        },
        {
            parameter: 'product_price',
            key: true,
            value: true,
            type:String,
            regular_expression:""
        },
        {
            parameter: 'product_quantity',
            key: true,
            value: true,
            type:Number,
            regular_expression:""
        },
        {
            parameter: 'product_quantity_type',
            key: true,
            value: true,
            type:String,
            regular_expression:""
        }
    ],

    update_product:[
        {
            parameter: 'product_name',
            key: true,
            value: true,
            type:String,
            regular_expression:""
        },
        {
            parameter: 'product_price',
            key: true,
            value: true,
            type:String,
            regular_expression:""
        },
        {
            parameter: 'product_quantity',
            key: true,
            value: true,
            type:Number,
            regular_expression:""
        },
        {
            parameter: 'product_quantity_type',
            key: true,
            value: true,
            type:String,
            regular_expression:""
        },
        {
            parameter: 'product_id',
            key: true,
            value: true,
            type:String,
            regular_expression:""
        }
    ]
}