var router = require('express').Router();
var user_handler = require("../handlers/user_handler");
var validate_parms = require("../middlewares/params_validation");
var jwt = require('jsonwebtoken');
var ejs = require('ejs');
var response_messages = require("../../../libraries/messages/response");
var request_parms = require("../middlewares/request_parameters");
var sms = require("../../../libraries/communication/sms");
var email = require("../../../libraries/communication/email");
var aws = require("../../../libraries/file_upload/aws");
var sms_messages = require("../../../libraries/messages/sms");
var vsprintf = require("sprintf-js").vsprintf;
var moment = require('moment');
var config = require("../../../../config");
var email_views_path = __dirname + "/../views/email";
var fs = require('fs');

router.use('/', function (request, response, next) {
    console.log('I am in middleware');
    next();
})

router.post('/create_user', validate_parms.checkBodyParameter(request_parms.registration), function (request, response) {
    config.console(request.body);
    user_handler.create_user_promise(request.body).then(function (success) {
        response.status(200).json({
            status: true,
            message: response_messages.registration_success,
            data: success
        });

    }).catch(function (error) {
        response.status(400).json({
            status: false,
            message: error.message,
            key: error.key
        });
    });
});

router.post('/request_user_otp', validate_parms.checkBodyParameter(request_parms.request_otp), function (request, response) {
    console.log(request.body);
    user_handler.send_user_otp_promise(request.body).then(function (success) {
        console.log(success);

        response.status(200).json({
            status: true,
            message: response_messages.request_otp
        });

    }).catch(function (error) {
        response.status(400).json({
            status: false,
            message: error.message,
            key: error.key
        });
    });
});

router.post('/verify_user_otp',
    validate_parms.checkBodyParameter(request_parms.verify_otp),
    function (request, response) {
        user_handler.verify_user_promise(request.body).then(function (success) {
            response.status(200).json({
                status: true,
                message: "Phone " + response_messages.verfy_success,
                data: success
            });
        }).catch(function (error) {
            response.status(400).json({
                status: false,
                message: error.message,
                key: error.key
            });
        });
    });


router.post('/login_user', validate_parms.checkBodyParameter(request_parms.login),
    function (request, response) {
        var request_obj = {
            phone: request.body.phone,
            password: request.body.password
        }
        user_handler.login_user_callback(request_obj, function (err, user) {
            config.console(err);
            if (err) {
                response.status(500).json({
                    status: false,
                    message: response_messages.db_error,
                    key: "DB"
                });
            } else {
                if(!user){
                    response.status(404).json({
                        status: false,
                        message: "User " + response_messages.does_not_exist,
                        key: "user"
                    });
                }

                if (!user.status.phone) {
                    response.status(404).json({
                        status: false,
                        message: "User " + response_messages.verfy_failure,
                        key: "verify"
                    });
                }
                else if (user_handler.compare_password(request_obj.password, user.password)) {
                    var payload = {
                        _id: user._id
                    };
                    var user_token = jwt.sign(payload, config.auth_token_secret, {
                        expiresIn: 1440 * 24 * 7
                    });

                    var user_details = {
                        name: user.name,
                        phone: user.phone,
                        role: user.user_role,
                        email: (user.email) ? user.email : "",
                        gender: (user.gender) ? user.email : "NA",
                        phone_verified: user.status.phone,
                        email_verified: user.status.email
                    };

                    //   SENDING EMAIL - START --------------------------------------

                    var mailOptions = {
                        to: "kiranbt@gomalon.com",
                        cc: "kiranbt@gomalon.com",
                        bcc: "kiranbt@gomalon.com",
                        subject: 'Welcome to GSOFT',
                        text: 'Welcome to GSOFT',
                        data: {
                            name: "Kiran B T"
                        },
                        template_path: email_views_path + "/user/welcome.ejs"
                    };
                    email.send(mailOptions);

                    //   SENDING EMAIL - END --------------------------------------



                    response.status(200).json({
                        status: true,
                        message: response_messages.login_success,
                        token: user_token,
                        data: {
                            user: user_details
                        }
                    });
                } else {
                    response.status(404).json({
                        status: false,
                        message: "Password " + response_messages.does_not_match,
                        key: 'password'
                    });
                }

            }
        });
    });


router.post('/reset_user_password',
    validate_parms.checkBodyParameter(request_parms.reset_user_password),
    function (request, response) {
        if (request.body.password != request.body.confirm_password) {
            response.status(400).json({
                status: false,
                message: "Password and Confirm Password " + response_messages.does_not_match,
                key: "password"
            });
        }
        user_handler.reset_user_password(request.body).then(function (success) {
            response.status(404).json({
                status: true,
                message: "Phone " + response_messages.verfy_success,
                data: success
            });
        }).catch(function (error) {
            response.status(400).json({
                status: false,
                message: error.message,
                key: error.key
            });
        });
    });

router.post('/get_user',
    validate_parms.checkBodyParameter(request_parms.get_user),
    function (request, response) {
        var token = request.body.token;
        jwt.verify(token, config.auth_token_secret, function (err, decoded) {
            if (err) {
                response.status(400).json({
                    success: false,
                    message: response_messages.authentication_failed,
                    key: "authenticate"
                });
            } else {
                request.decoded = decoded;
                config.console(decoded);
                user_handler.get_user_by_id(decoded._id, function (err, user) {
                    if (err) {
                        response.status(500).json({
                            status: false,
                            message: response_messages.db_error,
                            key: "DB"
                        });
                    } else if (user) {
                        response.status(200).json({
                            status: true,
                            data: {
                                user: user
                            }
                        });
                    } else {
                        response.status(404).json({
                            status: false,
                            message: "User " + response_messages.does_not_exist,
                            key: "user"
                        });
                    }

                });
            }
        });
    });

router.post('/update_user',
    validate_parms.checkBodyParameter(request_parms.update_user),
    function (request, response) {
        var request_obj = request.body;
        var token = request_obj.token;
        jwt.verify(token, config.auth_token_secret, function (err, decoded) {
            if (err) {
                response.json({
                    success: false,
                    message: response_messages.authentication_failed,
                    key: "authenticate"
                });
            } else {
                user_handler.get_user_by_id(decoded._id, function (err, user) {
                    if (err) {
                        response.status(500).json({
                            status: false,
                            message: response_messages.db_error,
                            key: "DB"
                        });
                    } else if (user) {

                        if (request_obj.email != "") {
                            user.email = request_obj.email;
                        }
                        if (request_obj.name != "") {
                            user.name = request_obj.name;
                        }
                        if (request_obj.gender != "") {
                            user.gender = request_obj.gender;
                        }
                        // config.console(user);
                        user_handler.update_uesr(user, user._id, function (err) {

                            config.console(err);
                            if (err) {
                                response.status(500).json({
                                    status: false,
                                    message: response_messages.db_error,
                                    key: "DB"
                                });

                            } else {
                                response.status(200).json({
                                    status: true,
                                    message: "User " + response_messages.updated_successfully,
                                });
                            }
                        });
                    } else {
                        response.status(404).json({
                            status: false,
                            message: "User " + response_messages.does_not_exist,
                            key: "user"
                        });
                    }

                });
            }
        });
    });

router.post('/upload_user_profile_pic', validate_parms.checkBodyParameter(request_parms.upload_user_profile_pic),
    function (request, response) {
        config.console(request.files);
        if (request.files == null) {
            return response.status(400).json({
                status: false,
                message: "Input parameter is missing!",
                key: "profile_pic"
            });
        } else if (!request.files.profile_pic) {
            return response.status(400).json({
                status: false,
                message: "Input parameter is missing!",
                key: "profile_pic"
            });
        }
        var request_obj = request.body;
        var token = request_obj.token;
        let user_profile_pic = request.files.profile_pic;

        jwt.verify(token, config.auth_token_secret, function (err, decoded) {
            if (err) {
                response.status(400).json({
                    success: false,
                    message: response_messages.authentication_failed,
                    key: "authenticate"
                });
            } else {
                user_handler.get_user_by_id(decoded._id, function (err, user) {
                    config.console(err);
                    if (err) {
                        response.status(500).json({
                            status: false,
                            message: response_messages.db_error,
                            key: "DB"
                        });
                    } else if (user) {
                        config.console(config.assets_path);
                        var profile_pic_url = config.assets_path + "/images/" + user._id + ".jpg";

                        user_profile_pic.mv(profile_pic_url, function (err) {
                            config.console(err);
                            if (err) {
                                response.status(500).json({
                                    status: false,
                                    message: response_messages.something_went_wrong,
                                    key: "something_went_wrong"
                                });
                            } else {
                                var _file = {
                                    destination_path: config.aws_s3.STORE_PATH + "/profile_pictures/" + user._id + ".jpg",
                                    currenk_file_data: user_profile_pic.data
                                }
                                aws.uploadToS3(_file, function (err, data) {
                                    user.profile_pic = data.key;
                                    if (err) {
                                        response.status(500).json({
                                            status: false,
                                            message: response_messages.something_went_wrong,
                                            key: "something_went_wrong"
                                        });
                                    } else {

                                        user_handler.update_uesr(user, user._id, function (err) {
                                            // config.console(err);
                                            if (err) {
                                                response.status(500).json({
                                                    status: false,
                                                    message: response_messages.db_error,
                                                    key: "DB"
                                                });
                                            }
                                            fs.unlinkSync(profile_pic_url);
                                        });
                                    }
                                });
                                response.status(200).json({
                                    status: true,
                                    message: "Profile Pic " + response_messages.updated_successfully,
                                });
                            }
                        });

                    } else {
                        response.status(400).json({
                            status: false,
                            message: "User " + response_messages.does_not_exist,
                            key: "user"
                        });
                    }

                });
            }
        });
    });



module.exports = router;