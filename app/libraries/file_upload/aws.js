var AWS = require('aws-sdk');
var config = require("../../../config");

let s3bucket = new AWS.S3({
    accessKeyId: config.aws_s3.IAM_USER_KEY,
    secretAccessKey: config.aws_s3.IAM_USER_SECRET,
    Bucket: config.aws_s3.BUCKET_NAME,
  });

exports.uploadToS3 = function(file,callback) { 
   var params = {
    Bucket: config.aws_s3.BUCKET_NAME,
    Key: file.destination_path,
    Body: file.currenk_file_data,
    ACL: 'public-read'
   };
   config.console(params);
   s3bucket.upload(params,callback);
 
}