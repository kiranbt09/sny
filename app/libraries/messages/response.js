module.exports = {
    success : 'Success',
    failure : 'Failure',
    something_went_wrong : 'Something went wrong! Please try after some time or contact admin.',
    already_exist: "Already Exist!",
    does_not_exist: "Does\'t Exist!",
    does_not_match : "Does\'t Match!",
    login_success:"Logged in Successfully!",
    verfy_success:"succesfully verified!",
    verfy_failure:"Not verified!,Please Verify your Phone number/Email",
    registration_success:"You have succesfully registered.",
    db_error:"Database Error",
    request_otp:"OTP sent to your registered Mobile/Email.",  
    password_changed : "Password successfully Changed.",
    authentication_failed: "Failed to authenticate token.",
    updated_successfully:"Updated Successfully!"
}