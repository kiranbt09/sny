var nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');
var config = require("../../../config");
var ejs = require('ejs');

 var email_config = {
  "host": "smtp.gmail.com",
  "port": 465,
  "secure": true,
  "service": "Gmail",
  "auth": {
    "user": config.email.id,
    "pass": config.email.password
  },
  "tls": {
    "rejectUnauthorized": false
  }
}
var transporter = nodemailer.createTransport(smtpTransport(email_config));
exports.send = function(mailOptions){
  // config.console(mailOptions);
    
ejs.renderFile(mailOptions.template_path, mailOptions.data, function (err, data) {
  config.console(err);
  mailOptions.html = data;
  // config.console(mailOptions);
  transporter.sendMail(mailOptions, function(error, info){
    if (error) {
      config.console(error);
    } else {
      config.console('Email sent: ' + info.response);
    }
  });
  
});
  

}