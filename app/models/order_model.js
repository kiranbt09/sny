// get an instance of mongoose and mongoose.Schema
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var moment = require('moment');

var OrderSchema = new Schema({
    business_name: {
        type: String, // User Full Name
        required: true
    },
    owner_name: {
        type: String, // User Full Name
        required: true
    },
    area_name: {
        type: String, // User Full Name
        required: true
    },
    password: {
        type: String, // Encrypted Password
        required: true
    },
    product_list :[{
        name: {
            type: String, // 10 Digit phone number
            required: true
        },
        price :{
            type: Number, // 10 Digit phone number
            required: true
        },
        quantity :{
            type: Number, // 10 Digit phone number
            required: true
        },
        quantity_type :{
            type: String, // 10 Digit phone number
            required: true
        }
    }],
    bill :{
        total_amount: {
            type: String, // 10 Digit phone number
            required: true
        },
        discount :{
            type: Number, // 10 Digit phone number
            required: true
        },
        payable :{
            type: Number, // 10 Digit phone number
            required: true
        }
    },

    phone: {
        type: String, // 10 Digit phone number
        required: true
    },
    status : {
            type: String, // 10 Digit phone number
            required: true
        },
    created: {
        type: String, // Record created date time with format => YYYY-MM-DD HH:mm:ss
        default: moment().format('YYYY-MM-DD HH:mm:ss')
    },
    modified: String // Date and time of record is updated or modified with format => YYYY-MM-DD HH:mm:ss
});
OrderSchema.pre('save', function (next) {
    if (!this.modified) this.modified = moment().format('YYYY-MM-DD HH:mm:ss');
    next();
});
module.exports = mongoose.model('order',OrderSchema);
