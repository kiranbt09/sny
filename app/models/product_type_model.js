// get an instance of mongoose and mongoose.Schema
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var moment = require('moment');

var CategorySchema = new Schema({
    name: {
        type: String, // User Full Name
        required: true
    },
    status: {
        type: Boolean, // 10 Digit phone number
        required: true
    },
    created: {
        type: String, // Record created date time with format => YYYY-MM-DD HH:mm:ss
        default: moment().format('YYYY-MM-DD HH:mm:ss')
    },
    modified: String // Date and time of record is updated or modified with format => YYYY-MM-DD HH:mm:ss
});
CategorySchema.pre('save', function (next) {
    if (!this.modified) this.modified = moment().format('YYYY-MM-DD HH:mm:ss');
    next();
});
module.exports = mongoose.model('category',CategorySchema);
