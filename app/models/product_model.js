// get an instance of mongoose and mongoose.Schema
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var moment = require('moment');

var ProductSchema = new Schema({
    product_name: {
        type: String, // User Full Name
        required: true
    },
    product_image_url: {
        type: String, // User Full Name
        required: true
    },
    product_price: {
        type: String, // User Full Name
        required: true
    },
    product_quantity: {
        type: Number, // Encrypted Password
        required: true
    },
    product_quantity_type: {
        type: String, // 10 Digit phone number
        required: true
    },
    product_type_id: {
        type:String,
        required: true
    },
    status: {
        type: Boolean, // 10 Digit phone number
        required: true
    },
    created: {
        type: String, // Record created date time with format => YYYY-MM-DD HH:mm:ss
        default: moment().format('YYYY-MM-DD HH:mm:ss')
    },
    modified: String // Date and time of record is updated or modified with format => YYYY-MM-DD HH:mm:ss
});
ProductSchema.pre('save', function (next) {
    if (!this.modified) this.modified = moment().format('YYYY-MM-DD HH:mm:ss');
    next();
});
module.exports = mongoose.model('product',ProductSchema);
