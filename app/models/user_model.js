// get an instance of mongoose and mongoose.Schema
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var moment = require('moment');

var UserSchema = new Schema({
    name: {
        type: String, // User Full Name
        required: true
    },
    password: {
        type: String, // Encrypted Password
        required: true
    },
    role: { // What is user role?? Why this required??
        type: Number, // 1=> Super Admin, 2 => Vendor/Manager/Customer
        default: 2
    },
    phone: {
        type: String, // 10 Digit phone number
        required: true
    },
    email: String, // User Email address
    status : {
        phone : {
            type: Boolean, // if true user verified his/her phone number
            default: false
        },
        email : {
            type: Boolean, // if true user verified his/her email address
            default: false
        },
        banned : {
            type: Boolean, // // if true user banned to access everything
            default: false
        }
    },
    otp : {
        email : {
            code : Number, // 4 Digit random Number (1000 - 9999)
            expiry : String // Expiry date and time with format => YYYY-MM-DD HH:mm:ss
        },
        phone : {
            code : Number, // 4 Digit random Number (1000 - 9999)
            expiry : String // Expiry date and time with format => YYYY-MM-DD HH:mm:ss
        }
    },
    gender: String, // keys are male/female
    profile_pic : String, // URL from storage
    last : {
        ip : String, // Last login network ip
        login : String, // Last Login date time with format => YYYY-MM-DD HH:mm:ss
        device : String // browser/android/ios
    },
    origin : {
        device : String, // browser/android/ios
        source : String // Direct/Social/Referral etc...
    },
    created: {
        type: String, // Record created date time with format => YYYY-MM-DD HH:mm:ss
        default: moment().format('YYYY-MM-DD HH:mm:ss')
    },
    modified: String // Date and time of record is updated or modified with format => YYYY-MM-DD HH:mm:ss
});
UserSchema.pre('save', function (next) {
    if (!this.modified) this.modified = moment().format('YYYY-MM-DD HH:mm:ss');
    next();
});
module.exports = mongoose.model('users',UserSchema);
